FROM docker.io/osixia/openldap:1.5.0

# HACK: the image is based on buster and enables buster-backports,
# but that repo is gone, so we must remove it before installing new packages
RUN sed -i /backports/d /etc/apt/sources.list \
    && apt-get -y update \
    && LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    bc

ADD bootstrap /container/service/slapd/assets/config/bootstrap
ADD environment /container/environment/01-custom
ADD utils /container/utils
