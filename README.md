# foxtrot ldap

A *f*ake LDAP server with WMF schemas on it based on https://github.com/osixia/docker-openldap for local development.

This was mostly copied from https://gerrit.wikimedia.org/g/labs/striker

The main purpose of this is to load a local LDAP server for lima-kilo (https://gitlab.wikimedia.org/repos/cloud/toolforge/lima-kilo)

# How to use it

Follow the steps:

 0) Deploy lima-kilo:

 Follow instructions at https://gitlab.wikimedia.org/repos/cloud/toolforge/lima-kilo

 1) Build the docker image:

```
$ docker build --tag foxtrot-ldap .
```

 2) Load the docker image into kind (or minikube)

```
$ kind load docker-image foxtrot-ldap:latest -n toolforge
```

 3) Deploy the component into your local kubernetes (lima-kilo):

```
$ ./deploy.sh
```

 4) At this point, hopefully, it should work!

```
user@lima-kilo:~$ kubectl -n foxtrot-ldap exec deploy/foxtrot-ldap -it -- /bin/bash
[..]
user@lima-kilo:~$ kubectl -n foxtrot-ldap exec deploy/foxtrot-ldap -- /container/utils/dump-servicegroup-tree.sh
[..]
```


## Oneliner:

TL;DR:

```
$ docker build --tag foxtrot-ldap . && kind load docker-image foxtrot-ldap:latest -n toolforge && ./deploy.sh
```
