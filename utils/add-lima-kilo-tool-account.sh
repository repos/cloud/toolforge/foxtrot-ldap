#!/usr/bin/env bash
#
# Usage: add-lima-kilo-tool-account.sh NAME UID GID [DN_OF_MAINTAINER]

PROJECT="toolsbeta"
HOME_ROOT="/data/project"
SHELL="/bin/bash"
TOOL=${1:?TOOL required}
NEW_UID=${2:?UID required}
NEW_GID=${3:?GID required}
BASE_DN="dc=wikimedia,dc=org"
GROUP_BASE_DN="ou=servicegroups,${BASE_DN}"
ACCOUNT_BASE_DN="ou=people,ou=servicegroups,${BASE_DN}"
ADMIN_DN="cn=admin,${BASE_DN}"
ADMIN_PASS="admin"
MEMBER_DN=${4:-$ADMIN_DN}

/usr/bin/ldapadd -x -D "${ADMIN_DN}" -w "${ADMIN_PASS}" <<LDIF
dn: cn=${PROJECT}.${TOOL},${GROUP_BASE_DN}
changetype: add
objectClass: groupOfNames
objectClass: posixGroup
objectClass: top
cn: toolsbeta.${TOOL}
gidNumber: ${NEW_GID}
member: ${MEMBER_DN}
LDIF

/usr/bin/ldapadd -x -D "${ADMIN_DN}" -w "${ADMIN_PASS}" <<LDIF
dn: cn=${PROJECT}.${TOOL},${ACCOUNT_BASE_DN}
changetype: add
objectClass: shadowAccount
objectClass: posixAccount
objectClass: person
objectClass: top
cn: ${PROJECT}.${TOOL}
sn: ${PROJECT}.${TOOL}
uid: ${PROJECT}.${TOOL}
uidNumber: ${NEW_UID}
gidNumber: ${NEW_GID}
homeDirectory: ${HOME_ROOT}/${TOOL}
loginShell: ${SHELL}
LDIF
