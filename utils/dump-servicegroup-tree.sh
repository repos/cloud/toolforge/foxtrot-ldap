#!/usr/bin/env bash
#
# Usage: dump-servicegroup-tree.sh

BASE_DN="dc=wikimedia,dc=org"
GROUP_BASE_DN="ou=servicegroups,${BASE_DN}"
ADMIN_DN="cn=admin,${BASE_DN}"
ADMIN_PASS="admin"
SEARCH_TERM="(objectclass=*)"

/usr/bin/ldapsearch -z 0 -D "${ADMIN_DN}" -w "${ADMIN_PASS}" -b "${GROUP_BASE_DN}" "${SEARCH_TERM}"
