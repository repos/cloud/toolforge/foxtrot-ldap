#!/usr/bin/env bash
# Hack a tool into the testing LDAP server setup with MediaWiki-Vagrant's
# role::striker.
#
# Usage: del-tool.sh NAME [DN_OF_MAINTAINER]

TOOL=${1:?TOOL required}
BASE_DN="dc=wikimedia,dc=org"
TOOL_BASE_DN="ou=servicegroups,${BASE_DN}"
ADMIN_DN="cn=admin,${BASE_DN}"
ADMIN_PASS="admin"

/usr/bin/ldapadd -x -D "${ADMIN_DN}" -w "${ADMIN_PASS}" <<LDIF
dn: cn=toolsbeta.${TOOL},${TOOL_BASE_DN}
changetype: delete

dn: uid=toolsbeta.${TOOL},ou=people,${TOOL_BASE_DN}
changetype: delete

dn: cn=runas-toolsbeta.${TOOL},ou=sudoers,cn=toolsbeta,ou=projects,${BASE_DN}
changetype: delete
LDIF
