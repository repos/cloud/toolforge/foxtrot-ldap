#!/bin/bash

set -eu

BASE_DIR=$(dirname $(realpath -s $0))
HELMCHART=${BASE_DIR}/helmchart
deploy_environment=${1:-}

if [ -z "${deploy_environment}" ] ; then
    project=$(cat /etc/wmcs-project 2>/dev/null || echo "local")
    deploy_environment=$project
fi

values_file=${HELMCHART}/values.yaml
helm upgrade --install -n foxtrot-ldap --create-namespace --force foxtrot-ldap ${HELMCHART} -f ${values_file}
